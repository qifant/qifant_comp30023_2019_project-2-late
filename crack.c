/*************************** HEADER FILES ***************************/
#include <memory.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "sha256.h"

#define FILE4   "pwd4sha256"
#define FILE6    "pwd6sha256"
#define FILEWORD          "common_passwords.txt"
#define MIN     32
#define MAX     127
#define TOTAL   95
#define MAXWORD             12


void hashWord(char * text, BYTE *hashed);
long int fileSize(const char *fileName);
int checkHashed(BYTE * hashed1, BYTE * hashed2);
void hashcpy(BYTE * hashed1, BYTE * hashed2);
void checkWord(char * word, BYTE ** hashed, int pwds);
char *repSub(char *str1, char *str2, char *str3);
int fileguess(BYTE ** hashed, int pwds,int count);
int stupidguess(BYTE ** hashed, int pwds, int count);
int getHashed(char * file, BYTE ** hashed, int lines);
void oneargc();
void twoargc(int count);
void threeargc(char * passwordfile, char * hashedfile);


//convert the plain text to hashed Bytes
void hashWord(char * text, BYTE *hashed){
	SHA256_CTX ctx;
	sha256_init(&ctx);
	sha256_fill(&ctx, text, strlen(text));
	sha256_final(&ctx, hashed);
}

//get the size of the file
long int fileSize(const char *fileName){
    struct stat st;
    if(stat(fileName,&st)==0){
        return (st.st_size);
	}
	return 0;
	
}

//check two hashed if same return 1, if not return 0
int checkHashed(BYTE * hashed1, BYTE * hashed2){
    for(int i = 0; i < SHA256_BLOCK_SIZE; i++){
		if(hashed1[i] != hashed2[i]){
			return 0;
		}
	}
	return 1;
}

//copying the hashed
void hashcpy(BYTE * hashed1, BYTE * hashed2){
	for(int i = 0; i < SHA256_BLOCK_SIZE; i++){
		hashed1[i] = hashed2[i];
	}
}

//check the word
void checkWord(char * word, BYTE ** hashed, int pwds){
    BYTE hash[SHA256_BLOCK_SIZE];
	//hash the word
    hashWord(word, hash);
	//printf("%s\n", word);
	//check through the hashed
    for(int i = 0; i < pwds; i++){
        if(checkHashed(hash, hashed[i])){
            printf("%s %d\n", word, i+1);
        }
    }
}

//this function replace the substring with str3
char *repSub(char *str1, char *str2, char *str3) {
    char *result;
    char *ins;
    char *tmp;
    int len_str2 = strlen(str2);
    int len_str3 = strlen(str3);
    int len_front;
    int count;

    if (!str1 || !str2)
        return NULL;
	
    if (len_str2 == 0)
        return NULL;
	//if str3 is empty
    if (!str3)
        str3 = "";
	
    ins = str1;
    for (count = 0; tmp = strstr(ins, str2); ++count) {
        ins = tmp + len_str2;
    }
	
    tmp = result = malloc(strlen(str1) + (len_str3 - len_str2) * count + 1);
    if (!result)
        return NULL;
    while (count--) {
        ins = strstr(str1, str2);
        len_front = ins - str1;
        tmp = strncpy(tmp, str1, len_front) + len_front;
        tmp = strcpy(tmp, str3) + len_str3;
        str1 += len_front + len_str2;
    }
    strcpy(tmp, str1);
    return result;
}

//add one character
int addOne(BYTE ** hashed, int pwds, int count, char * word){
	//printf("addOne\n");
	//add one number
	char buff[20];
	char temp [MAXWORD] = {'\0'};
	strcpy(temp, word);
	for (int i = 0; i < 10; i++){
		if (count == 0){
			break;
		}
		snprintf (buff, sizeof(buff), "%d",i);
		strcpy(temp,word);
		strcat(temp, buff);
		checkWord(temp, hashed, pwds);
		count--;
	}
	return count;
}

//remove one character
int removeOne(BYTE ** hashed, int pwds, int count, char * word){
	//printf("removeOne");
	char temp [MAXWORD] = {'\0'};
	//take out the last character
	if (count != 0){
		strncpy(temp,word,strlen(word)-1);
		checkWord(temp, hashed, pwds);
		count--;
	}
	return count;
}

//try remove the last two because so many end with er or ar
int removeTwo(BYTE ** hashed, int pwds, int count, char *word){
	//printf("removeTwo");
	char temp [MAXWORD] = {'\0'};
	//take out the last two character
	if (count != 0){
		strncpy(temp,word,strlen(word)-2);
		checkWord(temp, hashed, pwds);
		count--;
	}
	return count;
}

//check for birthday only for pwd4
int checkBday(BYTE ** hashed, int pwds, int count){
	//check year
	//add one number
	char buff[20];
	char temp [MAXWORD] = {'\0'};
	for (int year = 1900; year < 2020; year++){
		if (count == 0){
			break;
		}
		snprintf (buff, sizeof(buff), "%d",year);
		strcat(temp, buff);
		checkWord(temp, hashed, pwds);
		bzero(temp,strlen(temp));
		count--;
	}
	//check date
	for(int month = 0; month < 13; month++){
		if (count == 0){
			break;
		}
		for(int date = 0; date< 32; date++){
			if(count ==0){
				break;
			}
			if(month < 10){
				if(date < 10){
					snprintf (buff, sizeof(buff), "%d%d%d%d",0,month,0,date);
				}else{
					snprintf (buff, sizeof(buff), "%d%d%d",0,month,date);
				}
			}else{
				if(date < 10){
					snprintf (buff, sizeof(buff), "%d%d%d",month,0,date);
				}else{
					snprintf (buff, sizeof(buff), "%d%d",month,date);
				}
			}
			strcat(temp, buff);
			checkWord(temp, hashed, pwds);
			bzero(temp,strlen(temp));
			count--;
			
		}
	}
	
	return count;
}

//check special numbers
int specialNumber(BYTE ** hashed, int pwds, int count){
	char buff[20];
	char temp [MAXWORD] = {'\0'};
	for (int i = 0; i < 10; i++){
		if (count == 0){
			break;
		}
		snprintf (buff, sizeof(buff), "%d%d%d%d",i,i,i,i);
		strcat(temp, buff);
		checkWord(temp, hashed, pwds);
		bzero(temp,strlen(temp));
		count--;
	}
	
	for (int i = 0; i < 10; i++){
		if (count == 0){
			break;
		}
		snprintf (buff, sizeof(buff), "%d%d%d%d%d%d",i,i,i,i,i,i);
		strcat(temp, buff);
		checkWord(temp, hashed, pwds);
		bzero(temp,strlen(temp));
		count--;
	}
	return count;
}

//check integers first
int checkInt(BYTE ** hashed, int pwds, int count){
	char buff[20];
	char temp [MAXWORD] = {'\0'};
	//4 pwds
	for (int i = 0; i < 10; i ++){
		if(count == 0){
			break;
		}
		for(int j = 0; j < 10; j++){
			if(count == 0){
				break;
			}
			for(int k = 0; k < 10; k++){
				if(count == 0){
					break;
				}
				for(int m = 0; m < 10; m++){
					if(count == 0){
						break;
					}
					snprintf(buff,sizeof(buff),"%d%d%d%d",i,j,k,m);
					strcat(temp,buff);
					checkWord(temp,hashed,pwds);
					bzero(temp,strlen(temp));
					count--;
				}
			}
		}
	}
	
	//6 pwds
	
	for (int i = 0; i < 10; i ++){
		if(count == 0){
			break;
		}
		for(int j = 0; j < 10; j++){
			if(count == 0){
				break;
			}
			for(int k = 0; k < 10; k++){
				if(count == 0){
					break;
				}
				for(int m = 0; m < 10; m++){
					if(count == 0){
						break;
					}
					for(int n = 0; n < 10; n++){
						if (count == 0){
							break;
						}
						for (int o = 0; o < 10; o++){
							if (count == 0){
								break;
							}
							snprintf(buff,sizeof(buff),"%d%d%d%d%d%d",i,j,k,m,n,o);
							strcat(temp,buff);
							checkWord(temp,hashed,pwds);
							bzero(temp,strlen(temp));
							count--;
						}
					}
				}
			}
		}
	}
	
	return count;
}

//check if there is a substring
int checkSub(BYTE ** hashed, int pwds, int count, char * word, char * sub, char * rep){
	char temp [MAXWORD] = {'\0'};
	if (strstr(word, sub)){
		strcpy(temp,repSub(word, sub, rep));
		count = tryFitting(hashed, pwds, count, temp);
	}
	return count;
}

//considering the first letter capital or all capital case
int tryCapital(BYTE ** hashed, int pwds, int count, char * word){
	char temp [MAXWORD] = {'\0'};
	//try capital the first letter
	if(count!=0){
		if(word[0] >= 'a' && word[0] <= 'z'){
			temp[0] = word[0] -32;
			strcat(temp,&word[1]);
		}else if(word[0] >= 'A' && word[0] <= 'Z'){
			temp[0] = word[0] + 32;
			strcat(temp,&word[1]);
		}
		
		count = tryFitting(hashed, pwds, count, temp);
		count = tryReplace(hashed, pwds, count, temp);
	}
	bzero(temp,strlen(temp));
	
	//try capital last letter
	if(count != 0){
		int index = strlen(word)-1;
		char temp[MAXWORD] = {'\0'};
		if(word[index] >= 'a' && word[index] <= 'z'){
			strncpy(temp,word, index);
			temp[index] = word[index] - 32;
		}else if(word[index] >= 'A' && word[index] <= 'Z'){
			strncpy(temp,word, index);
			temp[index] = word[index] + 32;
		}
		count = tryFitting(hashed, pwds, count, temp);
		count = tryReplace(hashed, pwds, count, temp);
	}
	//all capital
	if(count!=0){
		for(int i = 0; i < strlen(word); i++){
			if(word[i] >= 'a' && word[i] <= 'z'){
				temp[i] = word[i] - 32;
			}else{
				temp[i] = word[i];
			}
		}
		count = tryFitting(hashed, pwds, count, temp);
		count = tryReplace(hashed,pwds,count,temp);
	}
	bzero(temp,strlen(temp));
	
	//all lower
	if(count!=0){
		for(int i = 0; i < strlen(word); i++){
			if(word[i] >= 'A' && word[i] <= 'Z'){
				temp[i] = word[i] + 32;
			}else{
				temp[i] = word[i];
			}
		}
		count = tryFitting(hashed, pwds, count, temp);
		count = tryReplace(hashed,pwds,count,temp);
	}
	
	return count;
}

//special replacement
int tryReplace(BYTE ** hashed, int pwds, int count, char * word){
	//printf("tryReplace\n");
	count = checkSub(hashed, pwds, count, word, "two", "2");
	count = checkSub(hashed, pwds, count, word, "for", "4");
	count = checkSub(hashed, pwds, count, word, "are", "r");
	count = checkSub(hashed, pwds, count, word, "you", "u");
	count = checkSub(hashed, pwds, count, word, "four", "4");
	count = checkSub(hashed, pwds, count, word, "six", "6");
	count = checkSub(hashed, pwds, count, word, "sev", "7");
	count = checkSub(hashed, pwds, count, word, "one", "1");
	count = checkSub(hashed, pwds, count, word, "ate", "8");
	count = checkSub(hashed, pwds, count, word, "at", "@");
	count = checkSub(hashed, pwds, count, word, "@", "at");
	count = checkSub(hashed, pwds, count, word, "please", "plz");
	count = checkSub(hashed, pwds, count, word, "your", "yr");
	count = checkSub(hashed, pwds, count, word, "yr", "ur");
	count = checkSub(hashed, pwds, count, word, "ur", "yr");
	count = checkSub(hashed, pwds, count, word, "gud", "good");
	count = checkSub(hashed, pwds, count, word, "good", "gud");
	count = checkSub(hashed, pwds, count, word, "to", "2");
	count = checkSub(hashed, pwds, count, word, "too", "2");
	count = checkSub(hashed, pwds, count, word, "see", "c");
	count = checkSub(hashed, pwds, count, word, "wait", "w8");
	count = checkSub(hashed, pwds, count, word, "before", "b4");
	
	count = checkSub(hashed, pwds, count, word, "t", "7");
	count = checkSub(hashed, pwds, count, word, "T", "7");
	count = checkSub(hashed, pwds, count, word, "7", "T");
	count = checkSub(hashed, pwds, count, word, "7", "t");
	return count;
}

//try to fit the word into 4 or 6
int tryFitting(BYTE ** hashed, int pwds, int count, char * word){
	//printf("tryFitting\n");
	if(count!=0){
		if (strlen(word) == 4){
			checkWord(word,hashed,pwds);
			count--;
		}else if(strlen(word)==6){
			checkWord(word,hashed,pwds);
			count--;
			count = removeTwo(hashed, pwds, count, word);
		}else if(strlen(word) == 3){
			count = addOne(hashed, pwds, count, word);
		}else if(strlen(word) == 5){
			count = removeOne(hashed, pwds, count, word);
			count = addOne(hashed, pwds, count, word);
		}else if(strlen(word) == 7){
			count = removeOne(hashed, pwds, count, word);
		}else if(strlen(word) == 8){
			count = removeTwo(hashed, pwds, count, word);
		}
	}
	return count;
}

//guess from the file with common words provided
int fileguess(BYTE ** hashed, int pwds,int count){
	//printf("fileguess");
    FILE *fp = fopen (FILEWORD, "r" );
    if (fp != NULL) {
        char word [MAXWORD] = {'\0'};
		
		while (fgets(word, sizeof(word), fp ) != NULL )
        {
			if(count == 0){
				break;
			}
			//take the last new line
            strtok(word, "\n");
			count = tryFitting(hashed, pwds, count, word);
			count = tryReplace(hashed, pwds, count, word);
			count = tryCapital(hashed, pwds, count, word);
			bzero(word, strlen(word));
        }
		
		//close the file
        fclose(fp);
    }
	return count;
}

//guess one by one
int stupidguess(BYTE ** hashed, int pwds, int count){
	
    char word [MAXWORD] = {'\0'};
	//4 digits
	for(int i = MIN; i < MAX; i++){
		for (int j = MIN; j < MAX; j++){
			for (int k = MIN; k < MAX; k++){
				for (int m = MIN; m < MAX; m++){
					if(count == 0){
						break;
					}
					word[0] = (char) i;
					word[1] = (char) j;
					word[2] = (char) k;
					word[3] = (char) m;
					checkWord(word, hashed, pwds);
					count--;
				}
			}
		}
	}
	//6 digits
	
	
    char word6 [MAXWORD] = {'\0'};//6 digits
	for(int i = MIN; i < MAX; i++){
		for (int j = MIN; j < MAX; j++){
			for (int k = MIN; k < MAX; k++){
				for (int m = MIN; m < MAX; m++){
					for (int n = MIN; n < MAX; n++){
						for ( int o = MIN; o < MAX; o++){
							if (count == 0){
								break;
							}
							word6[0] = (char) i;
							word6[1] = (char) j;
							word6[2] = (char) k;
							word6[3] = (char) m;
							word6[4] = (char) n;
							word6[5] = (char) o;
							checkWord(word6, hashed, pwds);
							count--;
							
						}
					}
				}
			}
		}
	}
	
	
	return count;
}

//gets the hashed Byte from the file given and store
int getHashed(char * file, BYTE ** hashed, int lines) {
    FILE * fp = fopen(file, "rb");
	BYTE line[SHA256_BLOCK_SIZE];
	int rd;
	
	if(fp != 0){
		while((rd = fread(line, 1, SHA256_BLOCK_SIZE, fp)) > 0){
			hashcpy(hashed[lines], line);
			lines++;
			bzero(line, SHA256_BLOCK_SIZE);
		}
	}
	fclose(fp);
	return lines;
}

//if there are only one input
void oneargc(){
	//printf("oneargc\n");
	int count = -1;
	//calculate the number of passwords
    int pwds = fileSize(FILE4)/SHA256_BLOCK_SIZE + fileSize(FILE6)/SHA256_BLOCK_SIZE;
	
    // get memory for the hashed
    BYTE ** hashed = (BYTE **)malloc(sizeof(BYTE *) * pwds);
	for(int i = 0; i < pwds; i++) {
		hashed[i] = (BYTE *)malloc(sizeof(BYTE *)* SHA256_BLOCK_SIZE);
	}

	//get the hashed from files
	int lines = 0;
    lines = getHashed(FILE4, hashed, lines);
    lines = getHashed(FILE6, hashed, lines);

	
	//run the file guess first based on the commonly used words
    count = fileguess(hashed, pwds,count);
	//check for special numbers
	count = specialNumber(hashed, pwds, count);
	//check for bday
	count = checkBday(hashed,pwds,count);
	//test integers first
	count = checkInt(hashed, pwds, count);
	//for the rest, run the stupid guess, takes long time
	count = stupidguess(hashed, pwds,count);

    // free all
    for (int i = 0; i < pwds; i++){
		free(hashed[i]);
	}
    free(hashed);
}

//if there are two inputs
void twoargc(int count){
	//calculate the number of passwords
    int pwds = fileSize(FILE4)/SHA256_BLOCK_SIZE + fileSize(FILE6)/SHA256_BLOCK_SIZE;
	
    // get memory for the hashed
    BYTE ** hashed = (BYTE **)malloc(sizeof(BYTE *) * pwds);
	for(int i = 0; i < pwds; i++) {
		hashed[i] = (BYTE *)malloc(sizeof(BYTE *)* SHA256_BLOCK_SIZE);
	}

	//get the hashed from files
	int lines = 0;
    lines = getHashed(FILE4, hashed, lines);
    lines = getHashed(FILE6, hashed, lines);

	//run the file guess first based on the commonly used words
    count = fileguess(hashed, pwds,count);
	//check for special numbers
	count = specialNumber(hashed, pwds, count);
	//check for bday
	count = checkBday(hashed,pwds,count);
	//test integers first
	count = checkInt(hashed, pwds, count);
	//for the rest, run the stupid guess, takes long time
	count = stupidguess(hashed, pwds,count);

	
    // free all
    for (int i = 0; i < pwds; i++){
		free(hashed[i]);
	}
    free(hashed);
}

//if there are three inputs
void threeargc(char * passwordfile, char * hashedfile){
	int pwds = fileSize(hashedfile)/SHA256_BLOCK_SIZE;
	
	//allocate memmory for the hashed
	BYTE ** hashed = (BYTE**) malloc (sizeof(BYTE*) * pwds);
	for (int i = 0; i < pwds; i ++){
		hashed[i] = (BYTE *)malloc (sizeof(BYTE) * SHA256_BLOCK_SIZE);
	}
	
	
	//read the hashed file
	int lines = 0;
	lines = getHashed(hashedfile, hashed, lines);
	//check the passwords
	FILE * fp = fopen(passwordfile, "rb");
	
	if( fp != NULL){
		char word[12] = {'\0'};
		
		//whenever there is a line
		while(fgets(word, MAXWORD, fp) != NULL){
			//take the new line
			strtok(word,"\n");
			checkWord(word, hashed, pwds);
			//set to nothing
			bzero(word, strlen(word));
		}
	}
}

int main(int argc, char* argv[]){
    if(argc == 1){
        oneargc();
    }else if(argc == 2){
		int count = atoi(argv[1]);
		twoargc(count);
    }else if(argc == 3){
		char * filename1 = argv[1];
		char * filename2 = argv[2];
        threeargc(filename1, filename2);
    }
	return 0;
}
