#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <ctype.h>
#define G 15
#define P 97



//used this instead of the power, but not sure why
int calculate(int g, int m, int p)
{
	int r;
	int y = 1;
	while (m > 0){
		r = m % 2;
		// fast exponention
		if (r == 1)
			y = (y*g) % p;
		g = g*g % p;
		m = m / 2;
	}
	return y;
}

//to calculate the hex value
int toInt(char a){
	if(isalpha(a)){
		a = a - 87;
	}else{
		a = a - 48;
	}
	return (int) a;
}

int main(int argc, char ** argv)
{
	char buffer[256];
	//get the sha256 file
    FILE* file = popen("openssl sha256 dh.c", "r");
    if(!file){
        printf("something wrong with your command or file\n");
        exit(0);
    }

    while(fgets(buffer, 256, file)){
		
    }
	
    pclose(file);
	//copy the file
    //printf("buffer = %s\n", buffer);

    char hashed[256];
	strcpy(hashed, buffer);
	
	//convert to integer
	int hex1 = toInt(hashed[14]);
	int hex2 = toInt(hashed[15]);
	//printf("%d\n", hex1);
	//printf("%d\n", hex2);
	//this b is for the one to calculate
	int b = hex1 * 16 + hex2;
	//printf("b = %d\n", b);
	//calculate the one to send
	int B = calculate(G, b, P);
	printf("B = %d\n", B);
	
	
	
	//time to establish connection
	int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent * server;

    portno = 7800;


    /* Translate host name into peer's IP address ;
     * This is name translation service by the operating system
     */
    server = gethostbyname("172.26.37.44");

    if (server == NULL)
    {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }

    /* Building data structures for socket */

    bzero((char *)&serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;

    bcopy(server->h_addr_list[0], (char *)&serv_addr.sin_addr.s_addr, server->h_length);

    serv_addr.sin_port = htons(portno);

    /* Create TCP socket -- active open
    * Preliminary steps: Setup: creation of active open socket
    */

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
    {
        perror("ERROR opening socket");
        exit(0);
    }

    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("ERROR connecting");
        exit(0);
    }
	printf("successfully creat socket\n");
	
    bzero(buffer, 256);
	
	//send name
	strcpy(buffer, "qifant\n");
	n = write(sockfd, buffer, strlen(buffer));
	printf("successfully sent name\n");
	
	//send the value of B
	bzero(buffer, 256);
	//put integer B into into buffer
	sprintf(buffer, "%d\n", B);
	n = write(sockfd,buffer, strlen(buffer));
	printf("successfully sent B\n");
	
	bzero(buffer, 256);
	//wait for the response
	n = read(sockfd, buffer, 255);
	
	if (n < 0){
        perror("ERROR reading from socket");
        exit(0);
    }
	
	//should receive a int
	int receive = atoi(buffer);
	//printf("receive = %d\n",receive);
	printf("successfully received");
	
	int key = calculate(receive, b, P);
	//printf("key = %d\n", key);
	
	bzero(buffer, 256);
	
	//confirm the key
	sprintf(buffer, "%d\n", key);
	n = write(sockfd, buffer, strlen(buffer));
	bzero(buffer, 256);
	
	//receive the result
	n = read(sockfd, buffer, 255);
	if (n < 0){
        perror("ERROR reading from socket");
        exit(0);
    }
	
	printf("result: %s\n", buffer);
	
	
}

